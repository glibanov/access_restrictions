<?php

require __DIR__ . '/vendor/autoload.php';

Core::getInstance()->setDevMode(false);
Core::getInstance()->setCacheEnabled(false);

$entityManager = Core::getInstance()->getEM('users');

//enum
$conn = $entityManager->getConnection();
$conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
