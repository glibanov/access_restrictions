<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Core extends Singleton
{
    const DEFAULT_DB = 'users';
    const DB_USER    = 'users';
    const DB_WP      = 'wp';

    protected $dev_mode;

    protected $cache_enabled;

    protected function __construct()
    {
        //$this->setDefaultEnv();

        \Util\LogPoint::getInstance()->start();

        \Util\Config::getInstance()->setConfig(__DIR__ . '/../config/config.yaml');
    }

    private function setDefaultEnv()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        ini_set('memory_limit', '256M');
    }

    /**
     * @return bool
     */
    public function isDevMode()
    {
        return (boolean) $this->dev_mode;
    }

    /**
     * @param bool $bool
     */
    public function setDevMode($bool = false)
    {
        $this->dev_mode = (boolean) $bool;
    }

    /**
     * @return bool
     */
    public function isCacheEnabled()
    {
        return (boolean) $this->cache_enabled;
    }

    /**
     * @param bool $bool
     */
    public function setCacheEnabled($bool = true)
    {
        $this->cache_enabled = (boolean) $bool;
    }

    /**
     * @return array
     */
    protected function getDSN($db = self::DEFAULT_DB)
    {
        return [
            "dbname"   => \Util\Config::getInstance()->get('db.'.$db.'.dbname'),
            "user"     => \Util\Config::getInstance()->get('db.'.$db.'.user'),
            "password" => \Util\Config::getInstance()->get('db.'.$db.'.password'),
            "host"     => \Util\Config::getInstance()->get('db.'.$db.'.host'),
            "port"     => \Util\Config::getInstance()->get('db.'.$db.'.port'),
            "driver"   => \Util\Config::getInstance()->get('db.'.$db.'.driver'),
        ];
    }

    /**
     * @return \Doctrine\ORM\Configuration
     */
    protected function getDoctrineConfig()
    {
        $config = Setup::createAnnotationMetadataConfiguration(array("classes/Entity"), $this->isDevMode());

        if ($this->isCacheEnabled()) {
            $cacheProvider = $this->getCacheProvider();
            $config->setMetadataCacheImpl($cacheProvider);
            $config->setQueryCacheImpl($cacheProvider);
            $config->setResultCacheImpl($cacheProvider);
            $config->setHydrationCacheImpl($cacheProvider);
        }

        return $config;
    }

    /**
     * @return \Doctrine\Common\Cache\CacheProvider
     */
    public function getCacheProvider()
    {
        return \Util\Cache::getInstance()->getCacheProvider();
    }

    /**
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function getEM($db = self::DEFAULT_DB)
    {
        if (!isset(static::$instances['em_'.$db])) {
            static::$instances['em_'.$db] = EntityManager::create($this->getDSN($db), $this->getDoctrineConfig());
        }
        return static::$instances['em_'.$db];
    }

    /**
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getDBAL($db = self::DEFAULT_DB)
    {
        if (!isset(static::$instances['dbal_'.$db])) {
            static::$instances['dbal_'.$db] = \Doctrine\DBAL\DriverManager::getConnection($this->getDSN($db), $this->getDoctrineConfig());
        }
        return static::$instances['dbal_'.$db];
    }
}