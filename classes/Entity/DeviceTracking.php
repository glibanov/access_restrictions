<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="device_tracking",
 *              indexes={
 *                  @Index(columns={"device_id"}),
 *                  @Index(columns={"user_id", "cell"}),
 *              },
 *              uniqueConstraints={
 *                  @UniqueConstraint(columns={"device_id"})
 *              })
 *
 *
 * insert into device_tracking (user_id, cell, ip_long, device_id)
 * select d.user_id, d.cell, if(s.ip regexp '[0-9\.]', inet_aton(s.ip), null) as ip_long, d.device_id from user_devices as d left join sessions as s on d.device_id = s.device_id;
 *
 */
class DeviceTracking
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $user_id;

    /**
     * @Column(type="smallint", nullable=false)
     */
    protected $cell;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $ip_long;

    /**
     * @Column(type="string", nullable=false)
     */
    protected $device_id;

    /**
     * @Column(type="boolean", options={"default":false})
     */
    protected $is_active;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $history;

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setCell($cell)
    {
        $this->cell = $cell;
    }

    public function getCell()
    {
        return $this->cell;
    }

    public function setIpLong($ip_long)
    {
        $this->ip_long = $ip_long;
    }

    public function getIpLong()
    {
        return $this->ip_long;
    }

    public function setDeviceId($device_id)
    {
        $this->device_id = $device_id;
    }

    public function getDeviceId()
    {
        return $this->device_id;
    }

    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    public function getIsActive()
    {
        return $this->is_active;
    }

    public function setHistory($history)
    {
        $this->history = $history;
    }

    public function getHistory()
    {
        return $this->history;
    }

    public function isActive()
    {
        return $this->getIsActive();
    }

    public function addHistory($data = [])
    {
        $history = unserialize($this->getHistory()) ?: [];
        array_unshift($history, array_merge($data, [
            'date' => date('H:i:s d-m-Y'),
            'ua' => \Util\Helper::getUserAgent(),
        ]));
        $this->setHistory(serialize($history));
    }
}