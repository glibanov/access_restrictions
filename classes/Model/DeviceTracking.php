<?php

namespace Model;

class DeviceTracking
{
    /**
     * 24 - xxx.xxx.xxx.***
     * 16 - xxx.xxx.***.***
     *  8 - xxx.***.***.***
     */
    const LEVEL_OF_VERIFY_IP = 24;

    protected $entity = null;

    /**
     * DeviceTracking constructor.
     * @param array $data
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __construct($data = [])
    {
        if (self::isValidData($data) === false) return;

        $this->getDeviceTracking($data);
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     * @throws \Doctrine\ORM\ORMException
     */
    protected function getRepo()
    {
        return \Core::getInstance()->getEM(\Core::DB_USER)->getRepository("Entity\DeviceTracking");
    }

    /**
     * @return \Entity\DeviceTracking
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param $data
     * @return \Entity\DeviceTracking|mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getDeviceTracking($data)
    {
        $this->entity = $this->getRepo()
            ->createQueryBuilder("d")
            ->select("d")
            ->where("d.device_id = :device_id")
            ->orWhere("d.user_id = :user_id AND d.cell = :cell")
            ->setParameters([
                "device_id" => $data['device_id'],
                "user_id"   => $data['user_id'],
                "cell"      => $data['cell']
            ])
            ->orderBy("d.cell", "ASC")
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if ($this->entity === null) {
            $entity = new \Entity\DeviceTracking();
            $entity->setUserId($data['user_id']);
            $entity->setCell($data['cell']);
            $entity->setDeviceId($data['device_id']);
            $entity->setIpLong(ip2long(\Util\IPv4::getCurrentIP()));
            $entity->setIsActive(true);
            $entity->addHistory([
                'device_id' => $data['device_id'],
                'ip'        => \Util\IPv4::getCurrentIP(),
            ]);
            \Core::getInstance()->getEM(\Core::DB_USER)->persist($entity);
            \Core::getInstance()->getEM(\Core::DB_USER)->flush();

            $this->entity = $entity;
        }

        return $this->entity;
    }

    /**
     * @param $device_id
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function verify($device_id)
    {
        $state = true;

        if ($this->getEntity() && $this->getEntity()->isActive() === false) {

            $test_ip = long2ip($this->getEntity()->getIpLong());

            $verify_ip = \Util\IPv4::getCurrentIP();

            $state = \Util\IPv4::match($verify_ip, $test_ip, self::LEVEL_OF_VERIFY_IP);

            //success
            //register the change of a state
            if ($state) {
                $this->getEntity()->setIsActive(true);
                $this->getEntity()->setDeviceId($device_id);
                //if ($test_ip !== $verify_ip) {
                    $this->getEntity()->setIpLong(ip2long($verify_ip));
                    $this->getEntity()->addHistory([
                        'device_id' => $this->getEntity()->getDeviceId(),
                        'ip'        => $verify_ip,
                    ]);
                //}
                \Core::getInstance()->getEM(\Core::DB_USER)->flush();
            }
        }

        return $state;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function setCurrentIp()
    {
        $current_iplong = ip2long(\Util\IPv4::getCurrentIP());

        if ($this->getEntity()->getIpLong() !== $current_iplong) {
            $this->getEntity()->setIpLong($current_iplong);
            \Core::getInstance()->getEM(\Core::DB_USER)->flush();
        }
    }

    /**
     * @return bool
     */
    protected function isActive()
    {
        return $this->getEntity() ? $this->getEntity()->isActive() : false;
    }

    /**
     * @param $data
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ErrorException
     */
    static public function process($data)
    {
        $deviceTracking = new static($data);

        if ($deviceTracking->isActive() === false && $deviceTracking->verify($data['device_id']) === false) {
            throw new \ErrorException("Произошла ошибка.\r\n Сессия с Вашего IP запрещена");
        } else {
            $deviceTracking->setCurrentIp();
        }
    }

    /**
     * @param $data
     * @return bool
     */
    static protected function isValidData($data)
    {
        return isset($data['user_id']) && isset($data['device_id']) && isset($data['cell']);
    }
}