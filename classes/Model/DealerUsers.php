<?php

namespace Model;

use Symfony\Component\HttpFoundation\Request;

class DealerUsers
{
    /**
     * @var array
     */
    protected $columns = [
        'u.login',
        'u.user_id',
        'devices',
        'is_active',
        'u.paid_for_diller'
    ];

    protected $order_condition = null;
    protected $search_condition = null;
    protected $limit_condition = null;
    protected $draw = null;

    /**
     * DealerUsers constructor.
     */
    public function __construct()
    {
        if (defined('USER_ID') === false) return false;

        if (defined('STANDART_PRODUCT_ID') === false) {
            define('STANDART_PRODUCT_ID', 32);
        }

        $this->defineMainParams($_GET);
    }

    /**
     * @param $params
     */
    protected function defineMainParams($params)
    {
        /** set draw */
        $this->draw = isset($params['draw']) ? intval($params['draw']) : 0;

        if ($params['search']['value']) {
            $this->setSearchCondition($params['search']['value']);
        }

        /** ser order */
        if (isset($params['order']) && is_array($params['order'])) {
            $this->setOrderCondition($params['order']);
        }

        /** set limit */
        if (isset($params['start']) && isset($params['length'])) {
            $this->setLimitCondition($params['start'], $params['length']);
        }
    }

    /**
     * @param $value
     */
    protected function setSearchCondition($value)
    {
        $this->search_condition = "AND (
            u.login LIKE '%".$value."%'
            || u.user_id LIKE '%".$value."%'
            || u.email LIKE '%".$value."%'
            || device.device_id LIKE '%".$value."%'
        )";
    }

    /**
     * @param $params
     */
    protected function setOrderCondition($params)
    {
        $orderBy = [];
        foreach($params as $order) {
            $idx = intval($order['column']);
            $column = $this->columns[$idx] ? : 0;
            $dir = $order['dir'] === 'asc' ? 'ASC' : 'DESC';
            $orderBy[$idx] = $column . ' ' . $dir;
        }
        $this->order_condition = 'ORDER BY ' . implode(', ', $orderBy);
    }

    /**
     * @param $indent
     * @param $length
     */
    protected function setLimitCondition($indent, $length)
    {
        $this->limit_condition = "LIMIT $indent, $length";
    }

    /**
     * @return string
     */
    protected function getFilteredUsersSql()
    {
        return "SELECT u.*, IF(access.access_id IS NULL, 0, 1) as is_active
            FROM 4itv_user as u
            LEFT JOIN 4itv_access AS access ON access.access_id = (
                SELECT a1.access_id
                FROM 4itv_access AS a1
                LEFT JOIN 4itv_product AS p1 ON p1.product_id = a1.product_id 
                LEFT JOIN 4itv_user_status AS s1 ON s1.user_id = a1.user_id AND s1.product_id = a1.product_id
                WHERE a1.user_id = u.user_id AND p1.product_id != '".STANDART_PRODUCT_ID."' AND ".time()." BETWEEN UNIX_TIMESTAMP(a1.begin_date) AND UNIX_TIMESTAMP(a1.expire_date)
                ORDER BY a1.expire_date DESC
                LIMIT 1
            )
            LEFT JOIN user_devices AS device ON device.user_id = u.user_id
            WHERE u.parent_user = " . USER_ID . " ".$this->search_condition;
    }

    /**
     * @return mixed
     */
    public function getFilteredUsers()
    {
        return \ConnectDB::instance('users')->select(
            $this->getFilteredUsersSql()."
            $this->order_condition
            $this->limit_condition
        ");
    }

    /**
     * @return int|null
     */
    public function getFilteredAmountUsers()
    {
        $query = \ConnectDB::instance('users')->query($this->getFilteredUsersSql());

        return $query ? $query->num_rows : null;
    }

    /**
     * @return int|null
     */
    public function getTotalAmountUsers()
    {
        $sql = "SELECT * FROM 4itv_user WHERE parent_user = ".USER_ID;

        $query = \ConnectDB::instance('users')->query($sql);

        return $query ? $query->num_rows : null;
    }

    /**
     * @return null|int
     */
    public function getDrawValue()
    {
        return $this->draw;
    }
}