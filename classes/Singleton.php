<?php

abstract class Singleton
{
    /**
     * Array of instances for all derived classes
     *
     * @var array
     */
    protected static $instances = array();

    /**
     * Method to access a singleton
     *
     * @return static
     */
    public static function getInstance()
    {
        $className = get_called_class();

        // Create new instance of the object (if it is not already created)
        if (!isset(static::$instances[$className])) {
            static::$instances[$className] = new $className();
        }

        return static::$instances[$className];
    }

    /**
     * Destruct and recreate singleton
     *
     * @return static
     */
    public static function resetInstance()
    {
        $className = get_called_class();

        // Create new instance of the object (if it is not already created)
        if (isset(static::$instances[$className])) {
            unset(static::$instances[$className]);
        }

        return static::getInstance();
    }
}