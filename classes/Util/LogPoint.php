<?php

namespace Util;

class LogPoint extends \Singleton
{
    protected $state = [
        'startTime' => null,
        'startMemory' => null,
        'time' => null,
        'memory' => null,
    ];

    public function start()
    {
        $this->state = [
            'startTime' => microtime(true),
            'startMemory' => memory_get_usage(),
            'time' => microtime(true),
            'memory' => memory_get_usage(),
        ];

        return $this;
    }

    public function update()
    {
        $this->state = array_merge($this->state, [
            'time' => microtime(true),
            'memory' => memory_get_usage(),
        ]);
    }

    public function point()
    {
        $this->display(
            round(microtime(true) - $this->state['time'], 4),
            \Util\Converter::byteToHuman(memory_get_usage() - $this->state['memory'])
        );

        $this->update();
    }

    public function total()
    {
        $this->display(
            round(microtime(true) - $this->state['startTime'], 4),
            \Util\Converter::byteToHuman(memory_get_usage() - $this->state['startMemory'])
        );
    }

    public function display($time = null, $memory = null)
    {
        echo '<span style="border:1px double red;padding:3px 5px;margin:2px;display:inline-block;">
                Spent time: ' . $time . ' Sec; Spent memory: ' . $memory . '
              </span><br>';
    }
}