<?php

namespace Util;

class Config extends \Singleton
{
    protected $config = [];

    /**
     * @param $path
     * @return bool
     */
    public function setConfig($path)
    {
        if (file_exists($path) && is_readable($path)) {
            $this->config = array_merge_recursive(
                $this->config,
                \Symfony\Component\Yaml\Yaml::parse(file_get_contents($path))
            );

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param $name
     * @return array|mixed|null
     */
    public function get($name)
    {
        $config = $this->getConfig();

        foreach (explode('.', $name) as $k) {
            if (isset($config[$k])) $config = $config[$k];
            else return null;
        }

        return $config;
    }
}