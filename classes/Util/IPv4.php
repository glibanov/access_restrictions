<?php

namespace Util;

class IPv4 extends \Util\IPv4\SubnetCalculator
{
    /**
     * 24 - xxx.xxx.xxx.***
     * 16 - xxx.xxx.***.***
     *  8 - xxx.***.***.***
     */
    const DEFAULT_LEVEL_OF_VERIFY_IP = 24;

    /**
     * @param $verified_ip
     * @param $test_ip
     * @param int $network_size
     * @return bool
     */
    static public function match($verified_ip, $test_ip, $network_size = self::DEFAULT_LEVEL_OF_VERIFY_IP)
    {
        if (self::validateIP($verified_ip) === false || self::validateIP($test_ip) === false) return false;

        $network_size = (int) $network_size;

        $subnetCalculator = new parent($test_ip, $network_size);

        $verified_ip2long = ip2long($verified_ip);

        $min_ip2long = ip2long($subnetCalculator->getNetworkPortion());

        return $verified_ip2long >> (32 - $network_size) === $min_ip2long >> (32 - $network_size);
    }

    /**
     * @param $ip
     * @return bool
     */
    static public function validateIP($ip)
    {
        return (boolean) filter_var($ip, FILTER_VALIDATE_IP);
    }

    /**
     * @return mixed
     */
    static public function getCurrentIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];

        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

        else $ip = $_SERVER['REMOTE_ADDR'];

        return $ip;
    }
}