<?php

namespace Util;

class Converter
{
    static function byteToHuman($bytes)
    {
        $bytes = max($bytes, 0);

        $base = ($bytes ? log($bytes) : 0) / log(1024);
        $suffix = array("b", "Kb", "Mb", "Gb", "Tb");

        $index = floor($base);

        return round(pow(1024, $base - floor($base)), 1) . $suffix[$index];
    }
}