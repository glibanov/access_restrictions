<?php

namespace Util;

class Cache extends \Singleton
{
    const CACHE_DRIVER = 'Memcache';

    /**
     * Get cache provider
     *
     * @return \Doctrine\Common\Cache\CacheProvider
     */
    public function getCacheProvider()
    {
        $cacheDriverName = '\\' . self::CACHE_DRIVER;
        $cacheDriver = new $cacheDriverName;
        $cacheDriver->addServer('localhost', 11211);

        $cacheProviderName = '\Doctrine\Common\Cache\\' . self::CACHE_DRIVER . 'Cache';
        $cacheProvider = new $cacheProviderName;
        $setCacheProviderMethod = 'set' . self::CACHE_DRIVER;
        $cacheProvider->$setCacheProviderMethod($cacheDriver);

        return $cacheProvider;
    }

    /**
     * Set cache item
     *
     * @param $id
     * @param $data
     * @param int $lifeTime
     * @return bool
     */
    public function set($id, $data, $lifeTime = 0)
    {
        return $this->getCacheProvider()->save($id, $data, $lifeTime);
    }

    /**
     * Get cache item
     *
     * @param $id
     * @return false|mixed
     */
    public function get($id)
    {
        return $this->getCacheProvider()->fetch($id);
    }

    /**
     * Remove cache item
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        return $this->getCacheProvider()->delete($id);
    }

    /**
     * Check of availability of cache item
     *
     * @param $id
     * @return bool
     */
    public function check($id)
    {
        return $this->getCacheProvider()->contains($id);
    }

    /**
     * Clear all cache
     */
    public function clear()
    {
        $this->getCacheProvider()->flushAll();
    }

    /**
     * Get human view of stats
     *
     * @return array|null
     */
    public function getStats()
    {
        $stats = $this->getCacheProvider()->getStats();

        array_walk($stats, function(&$v, $k){
            if (strpos($k, 'memory') !== false) {
                $v = \Util\Converter::byteToHuman($v);
            }
        });

        return $stats;
    }
}