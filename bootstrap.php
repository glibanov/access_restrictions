<?php

require __DIR__ . '/vendor/autoload.php';

Core::getInstance()->setDevMode(false);
Core::getInstance()->setCacheEnabled(true);